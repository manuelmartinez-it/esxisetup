**A PowerCLI script to quickly and easily perform the initial configuration of an ESXi Host.**

Feel free to provide any feedback or improvements to this script.

*Check back for any updates*

---

## Script Region Layout

Below is a description of how the regions are setup.

1. **Static Variables Defined -** This has the static variables for the environment as well as a switch statment for the different vCenters.
2. **User Prompts -** Here is where the user is prompted for all of the volatile information.
3. **Function to perform actions -** A few functions that will do a few different actions.
4. **vSwitch & vdSwitches -** These are the functions related to virtual switches.
5. **ESX Actions -** This area holds the functions that actual makes configurations changes on the ESXi host.
6. **To Fix -** You guessed it this is where I move functions that aren't working correctly or have be recently added to do something new.
7. **Do The Work -** Finally in this region I have all of the defined *functions* from other regions in the order that I would like them to be run.

---

## Script Functions

I found it helpful to set all the different actions I wanted to perform into functions, indent them, and comment each funtion to explain what it does.

I plan to update this script in the future to be able to use it within vRO and remove some of the prompts. Be sure to follow my [blog](https://ha-dads.com) for other scripts and information relating to the Datacenter/Infrastructure. 

---

